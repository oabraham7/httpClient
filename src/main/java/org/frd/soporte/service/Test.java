package org.frd.soporte.service;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.codec.binary.Base64;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public class Test{

	public static void main(String []args){
		Map<Integer,String> myMap = new HashMap<Integer,String>();
		myMap.put(1,"Eddie");
		myMap.put(2,"Omar");
		
		System.out.println(myMap.size());
		for(String s:myMap.values()){
			System.out.println(s);
		}
		
		String base64Chain = new String(Base64.encodeBase64("oabraham/Secreto".getBytes()));
		System.out.println(base64Chain);
	}
	
	@RequestMapping(value="/myFirstEndPoint",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public void testingSpringRESTWSImpl(){
		//Missing annotations to decompress JSON Requests & compress JSON Responses 
		
		//Do whatever you want here, after decompressing the JSON Request
	}
	
}
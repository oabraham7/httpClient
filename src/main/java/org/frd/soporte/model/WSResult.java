package org.frd.soporte.model;

public class WSResult {

	private int httpStatusCode;
	private String httpRespMsg;
	
	public int getHttpStatusCode() {
		return httpStatusCode;
	}
	public void setHttpStatusCode(int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
	public String getHttpRespMsg() {
		return httpRespMsg;
	}
	public void setHttpRespMsg(String httpRespMsg) {
		this.httpRespMsg = httpRespMsg;
	}	
}
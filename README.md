This GIT Repository contains a Maven Java EE project:

- Applications/Modules:

	1) httpClient       - Is the root name of the Maven Java application project developed by OA

- DB creation instructions (Liquibase)

	Note: For creating the DB, catalogs and other DB objects related to the project, Apache Maven 3.0+ needs to be installed on the System (CLI Mode).
	
	In order to run liquibase (Maven must be installed in CLI Production O.S.), then we just run the following command:

	> mvn clean install
	
	It is important to remember the dbManager/liquibase.properties file must contain the proper key values in order to connect to the DB and create the BD needed objects.

Steps:
	
	1) Go the root path of the application (example):
	
		C:\workspace\httpClient
	
	2) Comment the following tag (Opening and close) of pom.xml file within project dispersa.backend.jpa:

		<pluginManagement>
		
	3) Run the following command (All DB Objects will be created if the DB config is correct.):
	
		mvn clean package
	
- Java deployment instructions:

Before deployment, in order to build all the artifacts of the project the following maven command needs to be executed:

	mvn clean install

1) httpClient

	This Maven Java Project is the root name of the Maven application, this application. This 
	project was developed in Eclipse Neon and depends, uses or is related with the following runtime and development libraries, tools or local Maven Projects:
				 
	
		- JUnit 4.12
		- Slf4j 1.7.7
		- Maven 3.3.3
		- Java JDK 1.8+
		- Commons-codec 1.10
		- Commons-logging 1.2
		- Httpcore 4.4.5
		- Commons-httpclient 3.1
		- Spring-ws-core 2.4.0
		
Any doubt related to code, deployment, need for jars/libraries or configuration information contact Omar Abraham (oabraham@frd.org.mx)